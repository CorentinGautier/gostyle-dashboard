import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import axios from "axios";
import VueAxios from "vue-axios";
import Listbox from "primevue/listbox";
import InputText from "primevue/inputtext";
import Button from "primevue/button";
import Toast from "primevue/toast";
import ToastService from "primevue/toastservice";
import Menubar from "primevue/menubar";
import Dialog from "primevue/dialog";

import { library } from "@fortawesome/fontawesome-svg-core";
import { faEdit, faArchive, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faEdit, faArchive, faTrash);

Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.prototype.$api = "http://localhost:3000/qrcode/";

Vue.use(require("vue-moment"));
Vue.use(VueAxios, axios);
Vue.use(ToastService);
Vue.use(Toast);
Vue.use(Dialog);

Vue.component("Dialog", Dialog);
Vue.component("InputText", InputText);
Vue.component("ToastService", ToastService);
Vue.component("Listbox", Listbox);
Vue.component("Button", Button);
Vue.component("Toast", Toast);
Vue.component("Dialog", Dialog);
Vue.component("Menubar", Menubar);

Vue.config.productionTip = false;

import "primevue/resources/themes/nova-light/theme.css";
import "primevue/resources/primevue.min.css";
import "primeicons/primeicons.css";

import VueQArt from "vue-qart";
import vuetify from "./plugins/vuetify";
Vue.filter('formatDate', function (value) {
        var date = new Date(value)
        var dd = String(date.getDate()).padStart(2, '0');
        var mm = String(date.getMonth() + 1).padStart(2, '0');
        var yyyy = String(date.getFullYear());
        return dd + '-' + mm + '-' + yyyy;
});

new Vue({
    components: { VueQArt },
    router,
    vuetify,
    render: h => h(App)
}).$mount("#app");
