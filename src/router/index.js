import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Archive from "../views/Archive"
import ListNextPage from "../views/ListNextPromo"
import Connexion from "../views/Connexion"
import ls from 'local-storage'

Vue.use(VueRouter)


const routes = [{
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/archive',
        name: 'archive',
        component: Archive,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/nextPromo',
        name: 'listNextPromo',
        component: ListNextPage,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/connexion',
        name: 'connexion',
        component: Connexion
    },
    {
        path: '/AddQrCode',
        name: 'AddQrCode',
        props: true,
        component: () => import ( /* webpackChunkName: "AddQrCode" */ '../views/AddQrCode.vue'),
        meta: {
            requiresAuth: true
        }
    }
]

const router = new VueRouter({
    routes
})

router.beforeEach((to, from, next) => {
    const user = ls.get('user')
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (user == 'admin') {
            next()
        } else {
            next({ name: 'connexion', query: { redirect: to.fullPath } })
        }
    } else {
        next()
    }
})
export default router
