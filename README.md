# gostyle-dashboard

# info du dernier push :

[![pipeline status](https://gitlab.com/CorentinGautier/gostyle-dashboard/badges/master/pipeline.svg)](https://gitlab.com/CorentinGautier/gostyle-dashboard/commits/master)

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
